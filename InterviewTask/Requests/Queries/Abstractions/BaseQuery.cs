﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries.Abstractions
{
    public abstract class BaseQuery
    {
        public int Id { get; set; }

    }
}
