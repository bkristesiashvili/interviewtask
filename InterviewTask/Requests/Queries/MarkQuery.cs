﻿using InterviewTask.Requests.Queries.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries
{
    public sealed class MarkQuery : BaseQuery
    {
        public string NameGeo { get; private set; }

        public string NameEng { get; private set; }
    }
}
