﻿using InterviewTask.Requests.Queries.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries
{
    public sealed class OwnerQuery : BaseQuery
    {
        public string Firstname { get; set; }

        public string Middlename { get; private set; }

        public string Lastname { get; private set; }

        public string IdentityNumber { get; private set; }
    }
}
