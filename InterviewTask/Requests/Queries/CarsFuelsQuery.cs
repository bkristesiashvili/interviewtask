﻿using InterviewTask.Requests.Queries.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries
{
    public sealed class CarsFuelsQuery : BaseQuery
    {
        public FuelQuery Fuel { get; private set; }
    }
}
