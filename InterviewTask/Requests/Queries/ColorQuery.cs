﻿using InterviewTask.Requests.Queries.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries
{
    public sealed class ColorQuery : BaseQuery
    {
        public string Name { get; private set; }

        public string Code { get; private set; }
    }
}
