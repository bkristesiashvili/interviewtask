﻿using InterviewTask.Requests.Queries.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Queries
{
    public sealed class TransportQuery : BaseQuery
    {
        public string VIN { get; private set; }

        public string TransportNumber { get; private set; }

        public DateTime ManufactureDate { get; private set; }

        public string Image { get; private set; }

        public ModelQuery Model { get; private set; }

        public ColorQuery Color { get; private set; }

        public ICollection<CarsFuelsQuery> Fuels { get; private set; }

        public ICollection<CarsOwnersQuery> Owners { get; private set; }
    }
}
