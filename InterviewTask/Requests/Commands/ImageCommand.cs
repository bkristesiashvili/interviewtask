﻿using InterviewTask.Requests.Commands.Abstractions;

using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Commands
{
    public sealed class ImageCommand : BaseCommand
    {
        #region PROPERTIES

        public IFormFile Image { get; set; }

        #endregion
    }
}
