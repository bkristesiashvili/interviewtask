﻿using InterviewTask.Requests.Commands.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Commands
{
    public sealed class TransportEditCommand : BaseCommand
    {
        [Required]
        public string VIN { get; set; }

        [Required]
        [MaxLength(10)]
        public string TransportNumber { get; set; }

        [Required]
        public DateTime ManufactureDate { get; set; }

        public int ModelId { get; set; }

        public int ColorId { get; set; }

        public int OwnerId { get; set; }
    }
}
