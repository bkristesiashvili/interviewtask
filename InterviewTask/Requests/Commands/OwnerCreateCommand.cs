﻿using InterviewTask.Requests.Commands.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Commands
{
    public class OwnerCreateCommand : BaseCommand
    {
        public string Firstname { get; set; }

        public string Middlename { get; set; }

        public string Lastname { get; set; }

        public string IdentityNumber { get; set; }
    }
}
