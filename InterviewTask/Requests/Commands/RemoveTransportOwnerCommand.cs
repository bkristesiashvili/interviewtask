﻿using InterviewTask.Requests.Commands.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Requests.Commands
{
    public sealed class RemoveTransportOwnerCommand : BaseCommand
    {
        [Required]
        public int TransportId { get; set; }

        [Required]
        public int OwnerId { get; set; }
    }
}
