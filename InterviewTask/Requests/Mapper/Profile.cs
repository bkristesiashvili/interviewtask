﻿using AutoMapper;

using InterviewTask.Database.Entities;
using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;

namespace InterviewTask.Requests.Mapper
{
    public sealed class MapperProfile : Profile
    {
        public MapperProfile()
        {
            TransportMapping();
            ModelMapping();
            MarkMapping();
            FuelMapping();
            CarsFuelsMapping();
            OwnerMapping();
            CarsOwnersMapping();
            ColorMapping();
        }

        #region PRIVATE METHODS

        private void TransportMapping()
        {
            CreateMap<Car, TransportQuery>();
            CreateMap<TransportCreateCommand, Car>();
            CreateMap<TransportEditCommand, Car>();
        }

        private void ModelMapping()
        {
            CreateMap<Model, ModelQuery>();
        }

        private void MarkMapping()
        {
            CreateMap<Mark, MarkQuery>();
        }

        private void FuelMapping()
        {
            CreateMap<Fuel, FuelQuery>();
        }

        private void CarsFuelsMapping()
        {
            CreateMap<CarsFuels, CarsFuelsQuery>();
        }

        private void OwnerMapping()
        {
            CreateMap<Owner, OwnerQuery>();
            CreateMap<OwnerCreateCommand, Owner>();
        }

        private void CarsOwnersMapping()
        {
            CreateMap<CarsOwners, CarsOwnersQuery>();
        }

        private void ColorMapping()
        {
            CreateMap<Color, ColorQuery>();
        }

        #endregion
    }
}
