﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Helpers.Cultures
{
    public struct AvailableCultures
    {
        public const string GEORGIAN = "ka-GE";

        public const string ENGLISH = "en-US";

        public const string GERMAN = "de-DE";
    }
}
