﻿using InterviewTask.Helpers.Cultures;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InterviewTask.Middlewares
{
    public sealed class LocalizationMiddleware
    {
        private readonly RequestDelegate _next;

        public LocalizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var acceptedLanguage = httpContext.Request.Headers["Accept-Language"].ToString();
            var clientLang = acceptedLanguage.Split(',').FirstOrDefault();

            var currentLang = AvailableCultures.GEORGIAN;

            switch (clientLang)
            {
                case AvailableCultures.ENGLISH:
                    currentLang = clientLang;
                    break;
                case AvailableCultures.GERMAN:
                    currentLang = clientLang;
                    break;                
                default: break;
            }

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(currentLang);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;


            httpContext.Items["ClientLang"] = Thread.CurrentThread.CurrentCulture.Name;
            httpContext.Items["ClientCulture"] = Thread.CurrentThread.CurrentUICulture.Name;

            await _next(httpContext);
        }
    }
}
