﻿using InterviewTask.Extensions;
using InterviewTask.Responses;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InterviewTask.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public sealed class LoggerMiddleware
    {
        private readonly RequestDelegate requestDelegate;

        private readonly ILogger Logger;

        public LoggerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            requestDelegate = next;

            Logger = loggerFactory.CreateLogger("Request");
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await requestDelegate(httpContext);
            }
            catch(Exception e)
            {
                Logger.LogError($"Something went wrong: {e.Message}");
                await HandleGlobalExceptionAsync(httpContext, e);
            }
            finally
            {
                var log = $"\n" +
                    $"Method:        { httpContext.Request?.Method } \n" +
                    $"Path:            { httpContext.Request?.Path.Value } \n" +
                    $"Status Code: { httpContext.Response?.StatusCode } \n";

                Logger.LogInformation(log);
            }
        }

        private static async Task HandleGlobalExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            
            var erro = new ErrorResponse(exception.Message, context.Response.StatusCode);

            await context.Response.WriteAsync(erro.AsJSON());
        }
    }
}
