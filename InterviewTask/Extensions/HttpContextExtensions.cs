﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InterviewTask.Extensions
{
    public static class HttpContextExtensions
    {
        public static IPAddress GetClientIpAddress(this HttpContext httpContext)
        {
            return httpContext.Features.Get<IHttpConnectionFeature>().RemoteIpAddress;
        }
    }
}
