﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace InterviewTask.Extensions
{
    public static class IQueryableExtensions
    {
        public static IQueryable<TObject> OrderBy<TObject>(this IQueryable<TObject> source, string orderBy)
        {
            var type = typeof(TObject);

            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            //var foundProperty = properties.FirstOrDefault(p => p.Name.ToLower().Equals(orderBy));

            //var property = foundProperty ?? properties.First();

            var property = properties.FirstOrDefault(p => p.Name.ToLower().Equals(orderBy)) ?? 
                throw new Exception("Invalid Ordering Query!");

            var parameter = Expression.Parameter(type, "p");

            var propertyAccess = Expression.MakeMemberAccess(parameter, property);

            var orderByExp = Expression.Lambda(propertyAccess, parameter);

            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), "OrderBy", 
                new Type[] { type, property.PropertyType }, 
                source.Expression, 
                Expression.Quote(orderByExp));

            return source.Provider.CreateQuery<TObject>(resultExp);
        }
    }
}
