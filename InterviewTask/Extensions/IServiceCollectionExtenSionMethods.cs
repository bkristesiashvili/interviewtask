﻿using InterviewTask.Database;

using Microsoft.Extensions.DependencyInjection;

using System;

using AutoMapper;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InterviewTask.Services.Abstractions;
using InterviewTask.Services;

namespace InterviewTask.Extensions
{
    public static class IServiceCollectionExtenSionMethods
    {
        public static void InjectCustomDependencies(this IServiceCollection services)
        {
            try
            {
                services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

                services.AddSwaggerGen();

                services.AddTransient<IUnitOfRepositories, UnitOfRepositories>();

                services.AddTransient<ITransportServices, TransportService>();
                services.AddTransient<IOwnerServices, OwnerService>();
            }
            catch { throw; }
        }
    }
}
