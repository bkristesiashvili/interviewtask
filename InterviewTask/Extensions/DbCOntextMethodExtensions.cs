﻿using InterviewTask.Database.Entities.Abstractions;
using InterviewTask.Database.Seeder.Abstractions;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Extensions
{
    public static class DbCOntextMethodExtensions
    {
        public static void Seed<TEntity>(this ModelBuilder modelBuilder, IDataSeeder<TEntity> seeds)
            where TEntity: BaseEntity, new()
        {
            modelBuilder.Entity<TEntity>().HasData(seeds.ExecuteSeeder());
        }
    }
}
