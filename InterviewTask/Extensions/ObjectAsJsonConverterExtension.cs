﻿//using Newtonsoft.Json;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Extensions
{
    public static class ObjectAsJsonConverterExtension
    {
        public static string AsJSON(this object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
