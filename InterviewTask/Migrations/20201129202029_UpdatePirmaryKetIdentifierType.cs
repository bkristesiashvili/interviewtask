﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace InterviewTask.Migrations
{
    public partial class UpdatePirmaryKetIdentifierType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarColors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    color_name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    color_code = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarColors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarFuels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    fuel_name = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarFuels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarMarks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name_geo = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    name_eng = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarMarks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarOwners",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    first_name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    middle_name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true, defaultValue: ""),
                    last_name = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    id_num = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarOwners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CarModels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name_geo = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    name_eng = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: false),
                    MarkId = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarModels_CarMarks_MarkId",
                        column: x => x.MarkId,
                        principalTable: "CarMarks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    vin_code = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    transport_number = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    Manufacture_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    car_image = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    ModelId = table.Column<int>(type: "integer", nullable: true),
                    ColorId = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()"),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cars_CarColors_ColorId",
                        column: x => x.ColorId,
                        principalTable: "CarColors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Cars_CarModels_ModelId",
                        column: x => x.ModelId,
                        principalTable: "CarModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarsWithFuels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CarId = table.Column<int>(type: "integer", nullable: true),
                    FuelId = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarsWithFuels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarsWithFuels_CarFuels_FuelId",
                        column: x => x.FuelId,
                        principalTable: "CarFuels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarsWithFuels_Cars_CarId",
                        column: x => x.CarId,
                        principalTable: "Cars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarsWithOwners",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CarId = table.Column<int>(type: "integer", nullable: true),
                    OwnerId = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarsWithOwners", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarsWithOwners_CarOwners_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "CarOwners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarsWithOwners_Cars_CarId",
                        column: x => x.CarId,
                        principalTable: "Cars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "CarColors",
                columns: new[] { "Id", "color_code", "color_name" },
                values: new object[,]
                {
                    { 1, "1", "წიტელი" },
                    { 2, "2", "შავი" },
                    { 3, "3", "ლურჯი" },
                    { 4, "4", "მწვანე" },
                    { 5, "5", "ყვითელი" }
                });

            migrationBuilder.InsertData(
                table: "CarFuels",
                columns: new[] { "Id", "fuel_name" },
                values: new object[,]
                {
                    { 1, "ბენზინი" },
                    { 2, "დიზელი" },
                    { 3, "ბუნებრივი აირი" },
                    { 4, "თხევადი აირი" }
                });

            migrationBuilder.InsertData(
                table: "CarMarks",
                columns: new[] { "Id", "name_eng", "name_geo" },
                values: new object[,]
                {
                    { 5, "Ford", "ფორდი" },
                    { 3, "Maserati", "მაზერატი" },
                    { 4, "Dodge", "დოჯი" },
                    { 1, "Ferrari", "ფერარი" },
                    { 2, "Porsche", "პორშე" }
                });

            migrationBuilder.InsertData(
                table: "CarModels",
                columns: new[] { "Id", "MarkId", "name_eng", "name_geo" },
                values: new object[,]
                {
                    { 11, null, "F-150 Raptor", "F-150 Raptor" },
                    { 1, null, "F430", "F430" },
                    { 2, null, "ENZO", "ENZO" },
                    { 3, null, "MC20", "MC20" },
                    { 4, null, "Quattroporte VI GTS", "Quattroporte VI GTS" },
                    { 5, null, "GranCabrio", "GranCabrio" },
                    { 6, null, "718 Cayman S", "718 Cayman S" },
                    { 7, null, "718 Boxster S", "718 Boxster S" },
                    { 8, null, "Durango SRT", "Durango SRT" },
                    { 9, null, "Charger 2019", "Charger 2019" },
                    { 10, null, "Mustang", "Mustang" },
                    { 12, null, "GT", "GT" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarColors_color_code",
                table: "CarColors",
                column: "color_code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarColors_color_name",
                table: "CarColors",
                column: "color_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarFuels_fuel_name",
                table: "CarFuels",
                column: "fuel_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarMarks_name_eng",
                table: "CarMarks",
                column: "name_eng",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarMarks_name_geo",
                table: "CarMarks",
                column: "name_geo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarModels_MarkId",
                table: "CarModels",
                column: "MarkId");

            migrationBuilder.CreateIndex(
                name: "IX_CarModels_name_eng",
                table: "CarModels",
                column: "name_eng",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarModels_name_geo",
                table: "CarModels",
                column: "name_geo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarOwners_id_num",
                table: "CarOwners",
                column: "id_num",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ColorId",
                table: "Cars",
                column: "ColorId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ModelId",
                table: "Cars",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_transport_number",
                table: "Cars",
                column: "transport_number",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_vin_code",
                table: "Cars",
                column: "vin_code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarsWithFuels_CarId",
                table: "CarsWithFuels",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_CarsWithFuels_FuelId",
                table: "CarsWithFuels",
                column: "FuelId");

            migrationBuilder.CreateIndex(
                name: "IX_CarsWithOwners_CarId",
                table: "CarsWithOwners",
                column: "CarId");

            migrationBuilder.CreateIndex(
                name: "IX_CarsWithOwners_OwnerId",
                table: "CarsWithOwners",
                column: "OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarsWithFuels");

            migrationBuilder.DropTable(
                name: "CarsWithOwners");

            migrationBuilder.DropTable(
                name: "CarFuels");

            migrationBuilder.DropTable(
                name: "CarOwners");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "CarColors");

            migrationBuilder.DropTable(
                name: "CarModels");

            migrationBuilder.DropTable(
                name: "CarMarks");
        }
    }
}
