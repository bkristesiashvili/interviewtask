﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class Fuel : BaseEntity
    {
        public Fuel() => Cars = new HashSet<CarsFuels>();

        [Required]
        [Column("fuel_name")]
        [DataType(DataType.Text)]
        [MaxLength(15)]
        public string Name { get; set; }

        public virtual ICollection<CarsFuels> Cars { get; set; }
    }
}
