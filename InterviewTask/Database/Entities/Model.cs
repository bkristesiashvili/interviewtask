﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class Model : BaseEntity
    {
        public Model() => Cars = new HashSet<Car>();

        [Required]
        [Column("name_geo")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string NameGeo { get; set; }

        [Required]
        [Column("name_eng")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string NameEng { get; set; }

        public virtual Mark Mark { get; set; }

        public virtual ICollection<Car> Cars { get; set; }
    }
}
