﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class CarsOwners : BaseEntity
    {
        public virtual Car Car { get; set; }

        public virtual Owner Owner { get; set; }
    }
}
