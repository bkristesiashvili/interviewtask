﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class Owner : BaseEntity
    {
        public Owner() => Cars = new HashSet<CarsOwners>();

        [Required]
        [Column("first_name")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string Firstname { get; set; }

        [Column("middle_name")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string Middlename { get; set; }

        [Required]
        [Column("last_name")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string Lastname { get; set; }

        [Required]
        [Column("id_num")]
        [DataType(DataType.Text)]
        [MaxLength(20)]
        public string IdentityNumber { get; set; }

        public virtual ICollection<CarsOwners> Cars { get; set; }
    }
}
