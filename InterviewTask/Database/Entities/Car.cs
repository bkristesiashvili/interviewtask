﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class Car : BaseEntity
    {
        public Car()
        {
            Owners = new HashSet<CarsOwners>();
            Fuels = new HashSet<CarsFuels>();
        }

        [Required]
        [Column("vin_code")]
        [DataType(DataType.Text)]
        [MaxLength(25)]
        public string VIN { get; set; }

        [Required]
        [Column("transport_number")]
        [DataType(DataType.Text)]
        [MaxLength(10)]
        public string TransportNumber { get; set; }

        [Required]
        [Column("Manufacture_date")]
        public DateTime ManufactureDate { get; set; } 

        [Required]
        [Column("car_image")]
        [DataType(DataType.Text)]
        [MaxLength(255)]
        [DefaultValue("")]
        public string Image { get; set; }

        public virtual Model Model { get; set; }

        public virtual Color Color { get; set; }

        public virtual ICollection<CarsOwners> Owners { get; set; }

        public virtual ICollection<CarsFuels> Fuels { get; set; }
    }
}
