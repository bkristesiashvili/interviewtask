﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Entities
{
    public class Color : BaseEntity
    {
        [Required]
        [Column("color_name")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        [Column("color_code")]
        [DataType(DataType.Text)]
        [MaxLength(30)]
        public string Code { get; set; }

        public virtual ICollection<Car> Cars { get; set; }
    }
}
