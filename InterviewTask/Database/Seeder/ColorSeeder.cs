﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Seeder.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Seeder
{
    public sealed class ColorSeeder : IDataSeeder<Color>
    {
        #region PUBLIC METHODS

        public IEnumerable<Color> ExecuteSeeder()
        {
            return new List<Color>
            {
                new Color{ Id = 1, Name ="წიტელი", Code="1" },
                new Color{ Id = 2, Name ="შავი", Code="2" },
                new Color{ Id = 3, Name ="ლურჯი", Code="3" },
                new Color{ Id = 4, Name ="მწვანე", Code="4" },
                new Color{ Id = 5, Name ="ყვითელი", Code="5" },
            };
        }

        #endregion

    }
}
