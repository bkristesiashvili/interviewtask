﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Seeder.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Seeder
{
    public sealed class MarkSeeder : IDataSeeder<Mark>
    {
        public IEnumerable<Mark> ExecuteSeeder()
        {
            return new List<Mark>
            {
                new Mark{ Id = 1, NameGeo ="ფერარი", NameEng="Ferrari" },
                new Mark{ Id = 2, NameGeo ="პორშე", NameEng="Porsche" },
                new Mark{ Id = 3, NameGeo ="მაზერატი", NameEng="Maserati" },
                new Mark{ Id = 4, NameGeo ="დოჯი", NameEng="Dodge" },
                new Mark{ Id = 5, NameGeo ="ფორდი", NameEng="Ford" }
            };
        }
    }
}
