﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Seeder.Abstractions
{
    public interface IDataSeeder<TEntity>
        where TEntity: BaseEntity, new()
    {
        IEnumerable<TEntity> ExecuteSeeder();
    }
}
