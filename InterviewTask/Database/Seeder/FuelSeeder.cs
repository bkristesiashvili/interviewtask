﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Seeder.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Seeder
{
    public sealed class FuelSeeder : IDataSeeder<Fuel>
    {
        public IEnumerable<Fuel> ExecuteSeeder()
        {
            return new List<Fuel>
            {
                new Fuel{ Id = 1, Name ="ბენზინი" },
                new Fuel{ Id = 2, Name ="დიზელი" },
                new Fuel{ Id = 3, Name ="ბუნებრივი აირი" },
                new Fuel{ Id = 4, Name ="თხევადი აირი" },
            };
        }
    }
}
