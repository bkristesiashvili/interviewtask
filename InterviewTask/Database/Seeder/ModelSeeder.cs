﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Seeder.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Seeder
{
    public sealed class ModelSeeder : IDataSeeder<Model>
    {
        public IEnumerable<Model> ExecuteSeeder()
        {
            return new List<Model>
            {
                new Model{ Id=1, NameGeo ="F430", NameEng="F430" },
                new Model{ Id=2, NameGeo ="ENZO", NameEng="ENZO" },
                new Model{ Id=3, NameGeo ="MC20", NameEng="MC20" },
                new Model{ Id=4, NameGeo ="Quattroporte VI GTS", NameEng="Quattroporte VI GTS" },
                new Model{ Id=5, NameGeo ="GranCabrio", NameEng="GranCabrio" },
                new Model{ Id=6, NameGeo ="718 Cayman S", NameEng="718 Cayman S" },
                new Model{ Id=7, NameGeo ="718 Boxster S", NameEng="718 Boxster S" },
                new Model{ Id=8, NameGeo ="Durango SRT", NameEng="Durango SRT" },
                new Model{ Id=9, NameGeo ="Charger 2019", NameEng="Charger 2019" },
                new Model{ Id=10, NameGeo ="Mustang", NameEng="Mustang" },
                new Model{ Id=11, NameGeo ="F-150 Raptor", NameEng="F-150 Raptor" },
                new Model{ Id=12, NameGeo ="GT", NameEng="GT" },
            };
        }
    }
}
