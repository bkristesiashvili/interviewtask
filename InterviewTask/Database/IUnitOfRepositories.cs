﻿using InterviewTask.Database.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database
{
    public interface IUnitOfRepositories :IDisposable
    {
        #region PUBLIC PROPERTIES

        CarRepository CarRepository { get; }

        OwnerRepository OwnerRepository { get; }

        ModelRepository ModelRepository { get; }

        ColorRepository ColorRepository { get; }

        FuelRepository FuelRepository { get; }

        #endregion

        #region SAVE CHANGES

        bool Save();

        #endregion

    }
}
