﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Repositories.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories
{
    public sealed class CarRepository : BaseRepository<Car>
    {
        #region PROPERTIES & CONSTRUCTOR

        public CarRepository(InterviewDbContext context) :base(context) { }

        #endregion
    }
}
