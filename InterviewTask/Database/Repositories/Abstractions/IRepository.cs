﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories.Abstractions
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : BaseEntity, new()
    {
        bool CommandExecuted { get; }

        InterviewDbContext DbContext { get; }

        Task<IEnumerable<TEntity>> GetAll(string orderingQuery = default);

        Task<TEntity> GetById(int id);

        void Create(TEntity newEntity);

        void Update(TEntity updatedEntity);

        void Delete(int id, out TEntity deletedEntity);

        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> query = default,
            Expression<Func<TEntity, object>> orderBy = default, bool orderAsc = true);

        IEnumerable<TEntity> SortBy(IQueryable<TEntity> entities, string orderingQuery);

        int Count();

        bool Save();
    }
}
