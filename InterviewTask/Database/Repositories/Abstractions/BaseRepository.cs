﻿using InterviewTask.Database.Entities.Abstractions;
using InterviewTask.Extensions;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories.Abstractions
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity, new()
    {
        #region PROPERTIES & CONSTRUCTOR

        public BaseRepository(InterviewDbContext context)
        {
            DbContext = context;
            CommandExecuted = false;
        }

        public InterviewDbContext DbContext { get; }

        public bool CommandExecuted { get; private set; }

        #endregion

        #region PUBLIC VIRTUAL METHODS

        public virtual void Create(TEntity newEntity)
        {
            try
            {
                CheckDbContextNotNull();

                if (newEntity == null) throw new ArgumentException($"{ nameof(newEntity) }");

                DbContext.Set<TEntity>().Add(newEntity);

                CommandExecuted = true;
            }
            catch
            {
                CommandExecuted = false;
                throw;
            }
        }

        public virtual void Update(TEntity updatedEntity)
        {
            try
            {
                CheckDbContextNotNull();

                if (updatedEntity == null) throw new ArgumentNullException($"{ nameof(updatedEntity) }");

                DbContext.Entry(updatedEntity).State = EntityState.Modified;

                CommandExecuted = true;
            }
            catch
            {
                CommandExecuted = false;
                throw;
            }
        }

        public virtual void Delete(int id, out TEntity deletedEntity)
        {
            try
            {
                CheckDbContextNotNull();

                var entity = GetById(id).Result;

                deletedEntity = entity;

                if (entity == null) throw new Exception($"Record with ID:{ id } Not Found!");
                else
                {
                    DbContext.Set<TEntity>().Remove(entity);
                    CommandExecuted = true;
                }
            }
            catch
            {
                deletedEntity = null;
                CommandExecuted = false;
                throw;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll(string orderingQuery = default)
        {
            try
            {
                CheckDbContextNotNull();

                var entities = await Find();

                if (string.IsNullOrEmpty(orderingQuery) || string.IsNullOrWhiteSpace(orderingQuery)) return entities;

                return SortBy(entities.AsQueryable(), orderingQuery);
            }
            catch
            {
                CommandExecuted = false;
                throw;
            }
        }

        public virtual async Task<TEntity> GetById(int id)
        {
            try
            {
                CheckDbContextNotNull();

                var entity = (await Find(e => e.Id.Equals(id)))
                    .FirstOrDefault();

                if (entity == null) throw new Exception($"Record with ID:{ id } Not Found!");

                CommandExecuted = true;

                return entity;

            }
            catch
            {
                CommandExecuted = false;
                throw;
            }
        }

        public async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> query = default,
            Expression<Func<TEntity, object>> orderBy = default, bool orderAsc = true)
        {
            var records = query == null ?
                 await DbContext.Set<TEntity>().ToListAsync() :
                 await DbContext.Set<TEntity>().Where(query).ToListAsync();

            if (orderBy != null && !orderAsc) return records.OrderByDescending(orderBy.Compile()).ToList();
            else if (orderBy != null && orderAsc) return records.OrderBy(orderBy.Compile()).ToList();
            else if (orderBy == null && !orderAsc) return records.OrderByDescending(r => r.Id).ToList();

            return records.OrderBy(r => r.Id).ToList();
        }

        public IEnumerable<TEntity> SortBy(IQueryable<TEntity> entities, string orderBy)
        {
            if (!entities.Any())
                return entities;

            if (string.IsNullOrEmpty(orderBy) || string.IsNullOrWhiteSpace(orderBy))
                return entities;

            return entities.OrderBy(orderBy);
        }

        public virtual int Count()
        {
            CheckDbContextNotNull();

            return DbContext.Set<TEntity>().Count();
        }

        public bool Save()
        {
            try
            {
                if (!CommandExecuted) return false;

                CheckDbContextNotNull();

                return DbContext.SaveChanges() >= 0;
            }
            catch
            {
                CommandExecuted = false;
                throw;
            }
        }

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

        #region PRIVATE METHODS

        private void CheckDbContextNotNull()
        {
            CommandExecuted = false;
            if (DbContext == null) throw new ArgumentNullException($"{ nameof(DbContext) }");
        }

        #endregion
    }
}
