﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Repositories.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories
{
    public sealed class FuelRepository : BaseRepository<Fuel>
    {
        #region CONSTRUCTOR

        public FuelRepository(InterviewDbContext context) : base(context) { }

        #endregion
    }
}
