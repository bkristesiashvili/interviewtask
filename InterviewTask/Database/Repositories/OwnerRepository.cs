﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Repositories.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories
{
    public sealed class OwnerRepository : BaseRepository<Owner>
    {
        #region PROPERTIES & CONSTRUCTOR

        public OwnerRepository(InterviewDbContext context) : base(context) { }

        #endregion
    }
}
