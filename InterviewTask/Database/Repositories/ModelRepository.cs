﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Repositories.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.Repositories
{
    public sealed class ModelRepository : BaseRepository<Model>
    {
        #region CONSTRUCTOR

        public ModelRepository(InterviewDbContext context) : base(context) { }

        #endregion
    }
}
