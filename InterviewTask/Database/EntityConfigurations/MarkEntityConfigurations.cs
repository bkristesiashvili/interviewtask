﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class MarkEntityConfigurations : BaseEntityConfiguration<Mark> 
    {
        public override void Configure(EntityTypeBuilder<Mark> builder)
        {
            builder.HasIndex(mark => mark.NameGeo).IsUnique();
            builder.HasIndex(mark => mark.NameEng).IsUnique();

            builder.HasMany(m => m.Models).WithOne(m => m.Mark)
                .OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
