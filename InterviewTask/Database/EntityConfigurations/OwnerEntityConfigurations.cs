﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class OwnerEntityConfigurations : BaseEntityConfiguration<Owner>
    {
        public override void Configure(EntityTypeBuilder<Owner> builder)
        {
            builder.HasIndex(owner => owner.IdentityNumber).IsUnique();

            builder.Property(owner => owner.Middlename).HasDefaultValue(string.Empty)
                .ValueGeneratedOnAdd();

            builder.HasMany(c => c.Cars).WithOne(c => c.Owner)
                .OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
