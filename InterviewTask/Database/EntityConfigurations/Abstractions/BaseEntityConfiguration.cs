﻿using InterviewTask.Database.Entities.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace InterviewTask.Database.EntityConfigurations.Abstractions
{
    public abstract class BaseEntityConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity: BaseEntity, new()
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.Property(e => e.CreatedAt).HasDefaultValueSql("NOW()");
            builder.Property(e => e.UpdatedAt).HasDefaultValueSql("NOW()");
        }
    }
}
