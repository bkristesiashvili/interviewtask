﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class ColorEntityConfigurations : BaseEntityConfiguration<Color>
    {
        public override void Configure(EntityTypeBuilder<Color> builder)
        {
            builder.HasIndex(Color => Color.Name).IsUnique();
            builder.HasIndex(Color => Color.Code).IsUnique();

            builder.HasMany(c => c.Cars).WithOne(c => c.Color)
                .OnDelete(DeleteBehavior.SetNull);

            base.Configure(builder);
        }
    }
}
