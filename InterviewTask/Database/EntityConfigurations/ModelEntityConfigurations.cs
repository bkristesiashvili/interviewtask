﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class ModelEntityConfigurations : BaseEntityConfiguration<Model>
    {
        public override void Configure(EntityTypeBuilder<Model> builder)
        {
            builder.HasIndex(Model => Model.NameGeo).IsUnique();
            builder.HasIndex(Model => Model.NameEng).IsUnique();

            builder.HasMany(c => c.Cars).WithOne(m => m.Model)
                .OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
