﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class CarWithOwnersConfigurations :BaseEntityConfiguration<CarsOwners>
    {
        public override void Configure(EntityTypeBuilder<CarsOwners> builder)
        {
            base.Configure(builder);
        }
    }
}
