﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class CarEntityConfigurations : BaseEntityConfiguration<Car>
    {
        public override void Configure(EntityTypeBuilder<Car> builder)
        {
            builder.HasIndex(car => car.VIN).IsUnique();
            builder.HasIndex(car => car.TransportNumber).IsUnique();

            builder.Property(car => car.Image).HasDefaultValue(string.Empty)
                .ValueGeneratedOnAdd();

            builder.HasMany(f => f.Fuels).WithOne(f => f.Car)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(o => o.Owners).WithOne(o => o.Car)
                .OnDelete(DeleteBehavior.Cascade);

            base.Configure(builder);
        }
    }
}
