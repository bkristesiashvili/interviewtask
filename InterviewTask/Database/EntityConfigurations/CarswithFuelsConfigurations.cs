﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class CarswithFuelsConfigurations : BaseEntityConfiguration<CarsFuels>
    {
        public override void Configure(EntityTypeBuilder<CarsFuels> builder)
        {
            base.Configure(builder);
        }
    }
}
