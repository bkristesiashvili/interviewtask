﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.EntityConfigurations.Abstractions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database.EntityConfigurations
{
    public sealed class FuelEntityConfigurations : BaseEntityConfiguration<Fuel>
    {
        public override void Configure(EntityTypeBuilder<Fuel> builder)
        {
            builder.HasIndex(Fuel => Fuel.Name).IsUnique();

            builder.HasMany(f => f.Cars).WithOne(c => c.Fuel)
                .OnDelete(DeleteBehavior.Cascade);


            base.Configure(builder);
        }
    }
}
