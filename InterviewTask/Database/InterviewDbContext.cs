﻿using InterviewTask.Database.Entities;
using InterviewTask.Database.Entities.Abstractions;
using InterviewTask.Database.EntityConfigurations;
using InterviewTask.Database.Seeder;
using InterviewTask.Extensions;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database
{
    public sealed class InterviewDbContext : DbContext
    {
        public InterviewDbContext(DbContextOptions<InterviewDbContext> option) : base(option) { }

        public DbSet<Mark> CarMarks { get; set; }

        public DbSet<Model> CarModels { get; set; }

        public DbSet<Owner> CarOwners { get; set; }

        public DbSet<Color> CarColors { get; set; }

        public DbSet<Fuel> CarFuels { get; set; }

        public DbSet<Car> Cars { get; set; }

        public DbSet<CarsOwners> CarsWithOwners { get; set; }

        public DbSet<CarsFuels> CarsWithFuels { get; set; }


        #region PUBLIC OVERRIDED METHODS

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MarkEntityConfigurations());

            modelBuilder.ApplyConfiguration(new ModelEntityConfigurations());

            modelBuilder.ApplyConfiguration(new OwnerEntityConfigurations());

            modelBuilder.ApplyConfiguration(new ColorEntityConfigurations());

            modelBuilder.ApplyConfiguration(new FuelEntityConfigurations());

            modelBuilder.ApplyConfiguration(new CarEntityConfigurations());

            modelBuilder.ApplyConfiguration(new CarswithFuelsConfigurations());

            modelBuilder.ApplyConfiguration(new CarWithOwnersConfigurations());

            modelBuilder.Seed(new ColorSeeder());

            modelBuilder.Seed(new FuelSeeder());

            modelBuilder.Seed(new MarkSeeder());

            modelBuilder.Seed(new ModelSeeder());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries()
                .Where(e => e.Entity is BaseEntity && e.State == EntityState.Modified || e.State == EntityState.Added);

            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Modified)
                {
                    ((BaseEntity)entry.Entity).UpdatedAt = DateTime.Now;
                    ((BaseEntity)entry.Entity).CreatedAt = entry.OriginalValues.GetValue<DateTime>("CreatedAt");
                }
                else if (entry.State == EntityState.Added)
                {
                    ((BaseEntity)entry.Entity).UpdatedAt = DateTime.Now;
                    ((BaseEntity)entry.Entity).CreatedAt = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }

        #endregion
    }
}
