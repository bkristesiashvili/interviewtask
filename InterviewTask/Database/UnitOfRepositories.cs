﻿using InterviewTask.Database.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Database
{
    public sealed class UnitOfRepositories : IUnitOfRepositories
    {

        #region CONSTRUCTOR

        public UnitOfRepositories(InterviewDbContext context)
        {
            DbContext = context;

            InitializeRepositories();
        }

        #endregion

        #region  DBCONTEXT PROPERTY

        public InterviewDbContext DbContext { get; }

        #endregion

        #region PUBLIC IMPLEMENTED PROPERTIES

        public CarRepository CarRepository { get; private set; }

        public OwnerRepository OwnerRepository { get; private set; }

        public ModelRepository ModelRepository { get; private set; }

        public ColorRepository ColorRepository { get; private set; }

        public FuelRepository FuelRepository { get; private set; }

        #endregion

        #region DISPOSE METHOD

        public void Dispose()
        {
            DbContext.Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion

        #region SAVE METHOD

        public bool Save()
        {
            try
            {
                return DbContext.SaveChanges() >= 0;
            }
            catch { throw; }
        }

        #endregion

        #region PRIVATE METHODS

        private void InitializeRepositories()
        {
            if (DbContext == null) throw new ArgumentNullException($"{ nameof(DbContext) }");

            CarRepository = new CarRepository(DbContext);
            OwnerRepository = new OwnerRepository(DbContext);
            ModelRepository = new ModelRepository(DbContext);
            ColorRepository = new ColorRepository(DbContext);
            FuelRepository = new FuelRepository(DbContext);
        }

        #endregion
    }
}
