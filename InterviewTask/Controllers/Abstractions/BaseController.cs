﻿using AutoMapper;

using InterviewTask.Database;
using InterviewTask.Responses;
using InterviewTask.Responses.Filters.Abstractions;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Controllers.Abstractions
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected ActionResult Success(object data)
        {
            return Success(data, default);
        }

        protected ActionResult Success(object data, string message)
        {
            return Ok(new SuccessResponse(data, message));
        }

        protected ActionResult BadRequest(string message)
        {
            return BadRequest(new ErrorResponse(message));
        }

        protected ActionResult NotFound(string message)
        {
            return NotFound(new ErrorResponse(message, StatusCodes.Status404NotFound));
        }

        protected ActionResult PagedResponse<TData>(IEnumerable<TData> responseData, BaseFilter validFilter, int totalRecords)
        {
            var totalPages = totalRecords / (double)validFilter.PageSize;

            int roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

            var response = new PagedResponse<IEnumerable<TData>>(responseData, validFilter.Page, validFilter.PageSize,
                roundedTotalPages, totalRecords);

            return Ok(response);
        }
    }
}
