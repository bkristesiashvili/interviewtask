﻿using AutoMapper;

using InterviewTask.Controllers.Abstractions;
using InterviewTask.Database;
using InterviewTask.Database.Entities;
using InterviewTask.Extensions;
using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;
using InterviewTask.Responses;
using InterviewTask.Responses.Filters;
using InterviewTask.Services.Abstractions;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportsController : BaseController
    {
        #region PROPERTIES  & CONSTRUCTOR

        public TransportsController(ITransportServices transportService) => TransportService = transportService;

        public ITransportServices TransportService { get; }

        #endregion

        #region ACTIONS

        [HttpGet]
        public async Task<ActionResult> GetAllTransports([FromQuery] TransportParameters filter)
            => PagedResponse(await TransportService.GetTransports(filter), filter, TransportService.TotalRecords());

        [HttpGet("{id}")]
        public async Task<ActionResult> GetTransportById(int id)
            => Success(await TransportService.GetTransportById(id));

        [HttpGet("owners")]
        public async Task<ActionResult> GetTransportOwners(int transportId)
            => Success(await TransportService.GetTransportOwners(transportId));

        [HttpPost]
        public ActionResult CreateTransport([FromBody] TransportCreateCommand newTransport)
            => Success(TransportService.InsertNewTransport(newTransport));

        [HttpPut("{id}")]
        public ActionResult UpdateTransport(int id, [FromBody] TransportEditCommand updatedTransport)
            => Success(TransportService.EditTransportInfo(id, updatedTransport));

        [HttpPut("new_owner")]
        public ActionResult AddNewOwner([FromBody] AddNewTransportOwnerCommand newOwner)
            => Success(TransportService.AddOwner(newOwner.TransportId, newOwner.OwnerId));

        [HttpDelete("{id}")]
        public ActionResult DeleteTransport(int id)
            => Success(TransportService.DeleteTransport(id));

        [HttpDelete("remove_owner")]
        public ActionResult RemoveTransportOwner([FromBody] RemoveTransportOwnerCommand removeOwner)
            => Success(TransportService.RemoveOwner(removeOwner.TransportId, removeOwner.OwnerId));

        #endregion

        #region WORKING WITH IMAGES

        [HttpPost("upload")]
        public ActionResult AddImage(int transportId, [FromForm] ImageCommand image)
            => Success(TransportService.AddTransportImage(transportId, image));

        [HttpPut("EditImage")]
        public ActionResult UpdateImage(int transportId, [FromForm] ImageCommand image)
            => Success(TransportService.UpdateTransportImage(transportId, image));

        [HttpDelete("RemoveImage")]
        public ActionResult DeleteImage(int transportId)
            => Success(TransportService.DeleteImage(transportId));

        #endregion

    }
}
