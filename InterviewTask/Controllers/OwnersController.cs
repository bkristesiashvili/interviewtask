﻿using InterviewTask.Controllers.Abstractions;
using InterviewTask.Extensions;
using InterviewTask.Requests.Commands;
using InterviewTask.Responses.Filters;
using InterviewTask.Services.Abstractions;

using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace InterviewTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnersController : BaseController
    {
        #region PROPERTIES  & CONSTRUCTOR

        public OwnersController(IOwnerServices ownerService) => OwnerService = ownerService;

        public IOwnerServices OwnerService { get; }

        #endregion

        #region ACTIONS

        [HttpGet]
        public async Task<ActionResult> GetAllOwners([FromQuery] OwnerParameters filter)
        => PagedResponse(await OwnerService.GetAllOwners(filter), filter, OwnerService.Totalrecords());

        [HttpPost]
        public ActionResult CreateNewOwner([FromBody] OwnerCreateCommand newOwner) 
            => Success(OwnerService.AddNewOwner(newOwner));

        [HttpDelete("{id}")]
        public ActionResult DeleteOwner(int id) 
            => Success(OwnerService.DeleteOwner(id));

        #endregion
    }
}
