﻿using AutoMapper;

using InterviewTask.Database;
using InterviewTask.Database.Entities;
using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;
using InterviewTask.Responses.Filters;
using InterviewTask.Services.Abstractions;

using Microsoft.AspNetCore.Hosting;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Services
{
    public sealed class TransportService : BaseService, ITransportServices
    {
        #region CONSTANTS

        private static readonly string[] EXTENSIONS = { "image/jpg", "image/png", "image/jpeg", "image/bmp" };

        private const string IMAGE_DIRECTORY = "images";

        #endregion

        #region FIELDS

        private readonly IWebHostEnvironment Env;

        #endregion

        #region CONSTRUCTOR

        public TransportService(IUnitOfRepositories repositoryAccess, IMapper mapper, IWebHostEnvironment _env)
            : base(repositoryAccess, mapper) => Env = _env;

        #endregion

        #region PUBLIC METHODS

        public TransportQuery DeleteTransport(int id)
        {
            try
            {
                CheckDependecies();

                Repositories.CarRepository.Delete(id, out var deletedTransport);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(deletedTransport);
            }
            catch { throw; }
        }

        public TransportQuery EditTransportInfo(int transportId, TransportEditCommand updatedTransporInfo)
        {
            try
            {
                CheckDependecies();

                var transportExisted = Repositories.CarRepository.GetById(transportId)
                    .Result;

                Mapper.Map(updatedTransporInfo, transportExisted);

                var model = Repositories.ModelRepository.GetById(updatedTransporInfo.ModelId)
                    .Result;

                var color = Repositories.ColorRepository.GetById(updatedTransporInfo.ColorId)
                    .Result;

                Repositories.CarRepository.Update(transportExisted);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(transportExisted);
            }
            catch { throw; }
        }

        public TransportQuery InsertNewTransport(TransportCreateCommand newTransport)
        {
            try
            {
                var transport = Mapper.Map<Car>(newTransport);

                var model = Repositories.ModelRepository.GetById(newTransport.ModelId)
                    .Result;

                var color = Repositories.ColorRepository.GetById(newTransport.ColorId)
                    .Result;

                var owner = Repositories.OwnerRepository.GetById(newTransport.OwnerId)
                    .Result;

                var fuel = Repositories.FuelRepository.GetById(newTransport.FuelId)
                    .Result;

                transport.Model = model;
                transport.Color = color;
                transport.Fuels.Add(new CarsFuels
                {
                    Fuel = fuel,
                    Car = transport
                });
                transport.Owners.Add(new CarsOwners
                {
                    Owner = owner,
                    Car = transport
                });

                Repositories.CarRepository.Create(transport);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw; }
        }

        public async Task<IEnumerable<TransportQuery>> GetTransports(TransportParameters byFilter)
        {
            try
            {
                var cars =(await Repositories.CarRepository.GetAll(byFilter.OrderBy))
                    .Skip((byFilter.Page - 1) * byFilter.PageSize)
                    .Take(byFilter.PageSize);

                if ((bool)!cars?.Any())
                    return Mapper.Map<IEnumerable<TransportQuery>>(null);

                return Mapper.Map<IEnumerable<TransportQuery>>(cars);
            }
            catch { throw; }
        }

        public async Task<TransportQuery> GetTransportById(int id)
        {
            try
            {
                var transport = await Repositories.CarRepository.GetById(id);

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw; }
        }

        public async Task<IEnumerable<CarsOwnersQuery>> GetTransportOwners(int transportId)
        {
            try
            {
                var transport = await Repositories.CarRepository.GetById(transportId);

                return Mapper.Map<IEnumerable<CarsOwnersQuery>>(transport.Owners);
            }
            catch { throw; }
        }

        public TransportQuery AddOwner(int transportId, int OwnerId)
        {
            try
            {
                var transport = Repositories.CarRepository.GetById(transportId).Result;

                var owner = Repositories.OwnerRepository.GetById(OwnerId).Result;

                transport.Owners.Add(new CarsOwners
                {
                    Owner = owner,
                    Car = transport
                });

                Repositories.CarRepository.Update(transport);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw; }
        }

        public TransportQuery RemoveOwner(int transportId, int ownerId)
        {
            try
            {
                var transport = Repositories.CarRepository.GetById(transportId)
                    .Result;

                var owner = transport.Owners.FirstOrDefault(o => o.Owner.Id.Equals(ownerId));

                transport.Owners.Remove(owner);

                Repositories.CarRepository.Update(transport);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw;  }
        }

        public int TotalRecords()
        {
            try
            {
                return Repositories.CarRepository.Count();
            }
            catch { throw;  }
        }

        #region WORKING ON IMAGES

        public TransportQuery AddTransportImage(int transportId, ImageCommand newImage)
        {
            try
            {
                return ProcessImage(transportId, newImage, holdOld: true);
            }
            catch { throw; }
        }

        public TransportQuery UpdateTransportImage(int transportId, ImageCommand updatedImage)
        {
            try
            {
                return ProcessImage(transportId, updatedImage, holdOld: false);
            }
            catch { throw; }
        }

        public TransportQuery DeleteImage(int transportId)
        {
            try
            {
                var transport = Repositories.CarRepository.GetById(transportId)
                    .Result;

                DeleteFile(transport.Image);

                transport.Image = string.Empty;

                Repositories.CarRepository.Update(transport);

                Repositories.Save();

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw; }
        }

        #endregion

        #endregion

        #region PRIVATE METHODS

        private void CheckDependecies()
        {
            if (Repositories == null) throw new ArgumentNullException($"{ nameof(Repositories) }");
            if (Mapper == null) throw new ArgumentNullException($"{ nameof(Mapper) }");
        }

        private bool UploadImage(ImageCommand imageQuery, out string imagePath)
        {
            try
            {
                var formFile = imageQuery.Image;

                imagePath = string.Empty;

                if (!EXTENSIONS.Contains(formFile.ContentType))
                    throw new FormatException("File Format not supported!");

                var currentDir = Env.ContentRootPath;
                var imageDir = Path.Combine(currentDir, IMAGE_DIRECTORY);

                if (!Directory.Exists(imageDir)) Directory.CreateDirectory(imageDir);

                var randomName = $"{Guid.NewGuid():N}";

                var fileName = $"{randomName}{Path.GetExtension(formFile.FileName)}";

                imagePath = Path.Combine(IMAGE_DIRECTORY, fileName);

                var filePath = Path.Combine(imageDir,fileName);
                using Stream fileStream = new FileStream(filePath, FileMode.Create);
                formFile.CopyToAsync(fileStream).Wait();
                fileStream.FlushAsync().Wait();
                fileStream.DisposeAsync();

                return true;
            }
            catch { throw; }
        }

        private void DeleteFile(string oldFilePath)
        {
            try
            {
                var path = Path.Combine(Env.ContentRootPath, oldFilePath);                

                if (!File.Exists(path)) return;

                File.Delete(path);
            }
            catch { throw; }
        }

        private TransportQuery ProcessImage(int transportId, ImageCommand image, bool holdOld = false)
        {
            try
            {
                if (image.Image.Length <= 0)
                    throw new Exception("Image is a must!");

                var transport = Repositories.CarRepository.GetById(transportId)
                    .Result;

                var tempOldFilePath = transport.Image;

                if (!UploadImage(image, out var path))
                    throw new Exception("Image upload failed!");

                if (!holdOld) DeleteFile(tempOldFilePath);

                transport.Image = path;

                Repositories.CarRepository.Update(transport);
                Repositories.Save();

                return Mapper.Map<TransportQuery>(transport);
            }
            catch { throw; }
        }

        #endregion
    }
}
