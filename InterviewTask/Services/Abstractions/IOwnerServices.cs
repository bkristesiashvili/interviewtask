﻿using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;
using InterviewTask.Responses.Filters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Services.Abstractions
{
    public interface IOwnerServices
    {
        #region METHODS

        Task<IEnumerable<OwnerQuery>> GetAllOwners(OwnerParameters filter);

        OwnerQuery AddNewOwner(OwnerCreateCommand newOwner);

        OwnerQuery DeleteOwner(int id);

        int Totalrecords();

        #endregion
    }
}
