﻿using AutoMapper;

using InterviewTask.Database;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Services.Abstractions
{
    public interface IService
    {
        #region PROPERTIES

        IUnitOfRepositories Repositories { get; }

        IMapper Mapper { get; }

        #endregion
    }

    public abstract class BaseService : IService
    {
        #region PROPERTIES

        public IUnitOfRepositories Repositories { get; }

        public IMapper Mapper { get; }

        #endregion

        #region CONSTRUCTOR

        public BaseService(IUnitOfRepositories repositoryAccess, IMapper mapper)
        {
            Repositories = repositoryAccess;
            Mapper = mapper;
        }

        #endregion
    }
}
