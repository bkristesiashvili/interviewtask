﻿using AutoMapper;

using InterviewTask.Database;
using InterviewTask.Database.Entities;
using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;
using InterviewTask.Responses.Filters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Services.Abstractions
{
    public interface ITransportServices
    {
        #region METHODS

        TransportQuery InsertNewTransport(TransportCreateCommand transport);

        TransportQuery EditTransportInfo(int transportId, TransportEditCommand updatedTransporInfo);

        TransportQuery DeleteTransport(int id);

        Task<IEnumerable<TransportQuery>> GetTransports(TransportParameters byFilter);

        Task<TransportQuery> GetTransportById(int id);

        Task<IEnumerable<CarsOwnersQuery>> GetTransportOwners(int transportId);

        TransportQuery AddOwner(int transportId, int OwnerId);

        TransportQuery RemoveOwner(int transportId, int ownerId);

        int TotalRecords();

        TransportQuery AddTransportImage(int transportId, ImageCommand newImage);

        TransportQuery UpdateTransportImage(int transportId, ImageCommand updatedImage);

        TransportQuery DeleteImage(int transportId);

        #endregion
    }
}
