﻿using AutoMapper;

using InterviewTask.Database;
using InterviewTask.Database.Entities;
using InterviewTask.Requests.Commands;
using InterviewTask.Requests.Queries;
using InterviewTask.Responses.Filters;
using InterviewTask.Services.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Services
{
    public sealed class OwnerService : BaseService, IOwnerServices
    {
        #region CONSTRUCTOR

        public OwnerService(IUnitOfRepositories repositoryAccess, IMapper mapper)
            : base(repositoryAccess, mapper) { }

        #endregion

        #region PUBLIC METHODS

        public async Task<IEnumerable<OwnerQuery>> GetAllOwners(OwnerParameters byFilter)
        {
            try
            {
                CheckDependecies();

                var cars = (await Repositories.OwnerRepository.GetAll(byFilter.OrderBy))
                    .Skip((byFilter.Page - 1) * byFilter.PageSize)
                    .Take(byFilter.PageSize);

                if ((bool)!cars?.Any())
                    return Mapper.Map<IEnumerable<OwnerQuery>>(null);

                return Mapper.Map<IEnumerable<OwnerQuery>>(cars);
            }
            catch { throw; }
        }

        public OwnerQuery AddNewOwner(OwnerCreateCommand newOwner)
        {
            try
            {
                CheckDependecies();

                var owner = Mapper.Map<Owner>(newOwner);

                Repositories.OwnerRepository.Create(owner);

                Repositories.Save();

                return Mapper.Map<OwnerQuery>(owner);
            }
            catch { throw; }
        }

        public OwnerQuery DeleteOwner(int id)
        {
            try
            {
                CheckDependecies();

                Repositories.OwnerRepository.Delete(id, out var deletedOwner);

                Repositories.Save();

                return Mapper.Map<OwnerQuery>(deletedOwner);
            }
            catch { throw; }
        }

        public int Totalrecords()
        {
            return Repositories.OwnerRepository.Count();
        }

        #endregion

        #region PRIVATE METHODS

        private void CheckDependecies()
        {
            if (Repositories == null) throw new ArgumentNullException($"{ nameof(Repositories) }");
            if (Mapper == null) throw new ArgumentNullException($"{ nameof(Mapper) }");
        }

        #endregion
    }
}
