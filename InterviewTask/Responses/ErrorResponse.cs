﻿using InterviewTask.Responses.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Responses
{
    public sealed class ErrorResponse : BaseResponse<object>
    {
        #region CONSTRUCTOR

        public ErrorResponse(string message)
        {
            Message = message;
            Data = null;
            Success = false;
            StatusCode = 500;
        }

        public ErrorResponse(string message, int statusCode)
            :this(message)
        {
            StatusCode = statusCode;
        }

        #endregion
    }
}
