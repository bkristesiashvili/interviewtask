﻿namespace InterviewTask.Responses.Abstractions
{
    public abstract class BaseResponse<T> : IResponse<T>
    {
        #region CONSTRUCTORS

        public BaseResponse(T data, string message = default)
        {
            Data = data;
            Success = data == null ? false : true;
            Message = message;
        }

        public BaseResponse(string errorMessage) : this(default, errorMessage) { }

        public BaseResponse() : this(default) { }

        #endregion

        public T Data { get; protected set; }

        public bool Success { get; protected set; }

        public string Message { get; protected set; }

        public int StatusCode { get; protected set; }
    }
}
