﻿using InterviewTask.Database.Entities.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Responses.Abstractions
{
    public interface IResponse<T>
    {
        T Data { get; }

        bool Success { get; }

        string Message { get; }

        int StatusCode { get; }
    }
}
