﻿using InterviewTask.Responses.Filters.Abstractions;

using System.Linq;

namespace InterviewTask.Responses.Filters
{
    public sealed class TransportParameters : BaseFilter
    {
        public TransportParameters()
        {
            OrderBy = "vin";
        }
    }
}
