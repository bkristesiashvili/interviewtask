﻿using InterviewTask.Responses.Filters.Abstractions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Responses.Filters
{
    public sealed class OwnerParameters : BaseFilter
    {
        public OwnerParameters()
        {
            OrderBy = "firstname";
        }
    }
}
