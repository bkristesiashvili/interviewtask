﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterviewTask.Responses.Filters.Abstractions
{
    public abstract class BaseFilter
    {
        #region PRIVATE FIELDS

        #region CONSTANTS

        private const int MAX_PAGESIZE = 150;
        private const int MIN_PAGESIZE = 10;
        private const string DEFAULT_FILTER = "id";

        private const int MIN_PAGE_NUMBER = 1;

        #endregion

        private int _pageSize = MIN_PAGESIZE;

        private int _pageNumber = MIN_PAGE_NUMBER;

        private string _orderBy = DEFAULT_FILTER;

        #endregion

        public string OrderBy
        {
            get => _orderBy;
            set
            {
                _orderBy = string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value) ? DEFAULT_FILTER : value;
            }
        }

        public int Page
        {
            get => _pageNumber;
            set
            {
                _pageNumber = value < MIN_PAGE_NUMBER ? MIN_PAGE_NUMBER : value;
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value < MIN_PAGESIZE || value > MAX_PAGESIZE ? MIN_PAGESIZE : value;
            }
        }

        public BaseFilter()
        {
            Page = 1;
            PageSize = 10;
        }
    }
}
